-- Enchanted Wood
minetest.register_node("magicalities:tree_enchanted", {
	description = "Enchanted Tree",
	tiles = {"magicalities_tree_top.png", "magicalities_tree_top.png", "magicalities_table_wood.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, choppy = 2, oddly_breakable_by_hand = 1, flammable = 2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

-- circumvent a weird issue
local function add_fix(inv, item)
	inv:add_item("main", item)
end

-- Researchable bookshelf
-- Supposed to be a generated node that gives Research Notes
minetest.register_node("magicalities:bookshelf", {
	description = "Wise Bookshelf",
	tiles = {"default_wood.png", "default_wood.png", "default_wood.png",
		"default_wood.png", "default_bookshelf.png", "default_bookshelf.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
	drop = "default:bookshelf",
	on_rightclick = function (pos, node, clicker, itemstack, pointed_thing)
		if not clicker or clicker:get_player_name() == "" then return itemstack end
		local name = clicker:get_player_name()
		local count = math.random(0, 3)

		-- Swap the node with an ordinary bookshelf after inspecting
		node.name = "default:bookshelf"
		minetest.swap_node(pos, node)
		minetest.registered_nodes[node.name].on_construct(pos)

		-- A chance of not getting anything from this bookshelf
		if count == 0 then
			minetest.chat_send_player(name, "This bookshelf did not contain anything interesting.")
			return itemstack
		end

		-- Add some books into the new bookshelf
		local bookinv = minetest.get_meta(pos):get_inventory()
		bookinv:add_item("books", ItemStack("default:book " .. count))

		-- Give player Research Notes
		local item = ItemStack("magicalities:note")
		item:set_count(count)

		local inv = clicker:get_inventory()
		if inv:room_for_item("main", item) then
			minetest.after(0.1, add_fix, inv, item)
		else
			minetest.item_drop(item, clicker, clicker:get_pos())
		end

		minetest.chat_send_player(name, "You have read some of the books in this bookshelf and wrote up some notes.")

		return itemstack
	end
})

local nboxcollsel = {
	type = "fixed",
	fixed = {
		{-0.375, -0.5, 0.1875, -0.1875, -0.375, 0.375}, -- pot-base
		{-0.3125, -0.375, 0.25, -0.25, -0.3125, 0.3125}, -- pot-head
	}
}

local nbox = {
	type = "fixed",
	fixed = {
		{-0.375, -0.5, 0.1875, -0.1875, -0.375, 0.375}, -- pot-base
		{-0.3125, -0.375, 0.25, -0.25, -0.3125, 0.3125}, -- pot-head
		{-0.28125, -0.3125, 0.125, -0.28125, 0.1875, 0.4375}, -- feather
		{-0.5, -0.5, -0.5, 0.5, -0.5, 0.5}, -- paper-base
	}
}

minetest.register_node("magicalities:quill", {
	description = "Quill",
	tiles = {
		"magicalities_quill_top.png",
		"magicalities_quill_top.png^[transformFY",
		"magicalities_quill_right.png",
		"magicalities_quill_right.png^[transformFX",
		"magicalities_quill_front.png^[transformFX",
		"magicalities_quill_front.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	sunlight_propagates = true,
	paramtype2 = "facedir",
	is_ground_content = false,
	node_box = nbox,
	selection_box = nboxcollsel,
	collision_box = nboxcollsel,
	groups = { not_in_creative_inventory = 1 },
})

minetest.register_node("magicalities:quill_research", {
	tiles = {
		"magicalities_quill_top_research.png",
		"magicalities_quill_top.png^[transformFY",
		"magicalities_quill_right.png",
		"magicalities_quill_right.png^[transformFX",
		"magicalities_quill_front.png^[transformFX",
		"magicalities_quill_front.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	sunlight_propagates = true,
	paramtype2 = "facedir",
	is_ground_content = false,
	drop = "magicalities:quill",
	node_box = nbox,
	selection_box = nboxcollsel,
	collision_box = nboxcollsel,
	groups = { not_in_creative_inventory = 1 },
})


--tellium block kristovish
minetest.register_node("magicalities:tellium_crystal_block", {
	description = "Tellium Crystal Block",
	tiles = {"magicalities_telliumblock.png"},
	light_source = 7,
	paramtype = "light",
	is_ground_content = false,
	groups = {cracky = 3},
	sounds = default.node_sound_glass_defaults()
})

minetest.register_craft({
	output = "magicalities:tellium_crystal_block",
	recipe = {
		{"magicalities:tellium", "magicalities:tellium", "magicalities:tellium"},
		{"magicalities:tellium", "default:mese_crystal", "magicalities:tellium"},
		{"magicalities:tellium", "magicalities:tellium", "magicalities:tellium"}
	}
})


--tellium marble kristovish
minetest.register_node("magicalities:tellium_marble", {
	description = "Magical Marble",
	tiles = {"magicalities_tellium_marble.png"},
	--light_source = 7,
	paramtype = "light",
	is_ground_content = false,
	groups = {cracky = 3},
	sounds = default.node_sound_stone_defaults()
})

minetest.register_craft({
	output = "magicalities:tellium_marble 6",
	recipe = {
		{"magicalities:tellium", "magicalities:tellium", "magicalities:tellium"},
		{"magicalities:tellium", "magicalities:transterra", "magicalities:tellium"},
		{"magicalities:tellium", "magicalities:tellium", "magicalities:tellium"}
	}
})


--transterra lamp kristovish
minetest.register_node("magicalities:transterra_lamp", {
	description = "Transterra Lamp",
	tiles = {"magicalities_transterra_lamp.png"},
	light_source = 14,
	paramtype = "light",
	is_ground_content = false,
	groups = {cracky = 3},
	sounds = default.node_sound_glass_defaults()
})

minetest.register_craft({
	output = "magicalities:transterra_lamp",
	recipe = {
		{"magicalities:transterra", "magicalities:transterra", "magicalities:transterra"},
		{"magicalities:transterra", "default:mese_crystal", "magicalities:transterra"},
		{"magicalities:transterra", "magicalities:transterra", "magicalities:transterra"}
	}
})

--transterra glass kristovish
minetest.register_node("magicalities:transterra_glass", {
	description = "Transterra Glass",
	drawtype = "glasslike",
	tiles = {"magicalities_transterra_glass.png"},
	--light_source = 1,
	paramtype = "light",
	is_ground_content = false,
	sunlight_propagates = true,
	groups = {cracky=3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_glass_defaults()
})

minetest.register_craft({
	type = "cooking",
	output = "magicalities:transterra_glass",
	recipe = "magicalities:transterra"
})

--transterra block kristovish
minetest.register_node("magicalities:transterra_block", {
	description = "Transterra Block",
	--drawtype = "glasslike",
	tiles = {"magicalities_transterra_block.png"},
	light_source = 14,
	paramtype = "light",
	is_ground_content = false,
	groups = {cracky = 3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_stone_defaults()
})

minetest.register_craft({
	output = "magicalities:transterra_block",
	recipe = {
		{"magicalities:transterra", "goblins:moss", "magicalities:transterra"},
		{"basic_materials:brass_ingot", "default:mese_crystal", "basic_materials:brass_ingot"},
		{"magicalities:transterra", "goblins:moss", "magicalities:transterra"}
	}
})

--- silex kristovish
minetest.register_node("magicalities:silex", {
	description = "Silex Block",
	--drawtype = "glasslike",
	tiles = {"magicalities_silex.png"},
	light_source = 3,
	paramtype = "light",
	is_ground_content = false,
	groups = {cracky = 3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_stone_defaults()
})

minetest.register_craft({
	output = "magicalities:silex",
	recipe = {
		{"default:flint", "default:flint", "default:flint"},
		{"default:flint", "default:mese_crystal", "default:flint"},
		{"default:flint", "default:flint", "default:flint"}
	}
})
