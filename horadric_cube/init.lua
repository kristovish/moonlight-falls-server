S = minetest.get_translator("horadric_cube")

local default_path = minetest.get_modpath("horadric_cube")

dofile(default_path.."/cube.lua")
dofile(default_path.."/recipes.lua")
