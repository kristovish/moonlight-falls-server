cabs_table["simple_wooden_bedside_table"] = {
    ["simple_wooden_bedside_table_1"] = {
        {mode="closed", button = "sw_bedt1_1", img_button = "open_button.png"},
        {mode="closed", button = "sw_bedt1_2", img_button = "open_button.png"},
        {mode="closed", button = "sw_bedt1_3", img_button = "open_button.png"},
        
    },
    ["simple_wooden_bedside_table_2"] = {
        {mode="opened", button = "sw_bedt2_1", img_button = "close_button.png", listname = "sw_bedt2_1", inv_size=6*2},
        {mode="closed", button = "sw_bedt2_2", img_button = "open_button.png"},
        {mode="closed", button = "sw_bedt2_3", img_button = "open_button.png"},
        not_in_creative_inventory=1
        
    },
    ["simple_wooden_bedside_table_3"] = {
        {mode="closed", button = "sw_bedt3_1", img_button = "open_button.png"},
        {mode="opened", button = "sw_bedt3_2", img_button = "close_button.png", listname = "sw_bedt3_2", inv_size=6*2},
        {mode="closed", button = "sw_bedt3_3", img_button = "open_button.png"},
        not_in_creative_inventory=1
        
    },
    ["simple_wooden_bedside_table_4"] = {
        {mode="closed", button = "sw_bedt4_1", img_button = "open_button.png"},
        {mode="closed", button = "sw_bedt4_2", img_button = "open_button.png"},
        {mode="opened", button = "sw_bedt4_3", img_button = "close_button.png", listname = "sw_bedt4_3", inv_size=6*2},
        not_in_creative_inventory=1
        
    },
    ["simple_wooden_bedside_table_5"] = {
        {mode="opened", button = "sw_bedt5_1", img_button = "close_button.png", listname = "sw_bedt5_1", inv_size=6*2},
        {mode="closed", button = "sw_bedt5_2", img_button = "open_button.png"},
        {mode="opened", button = "sw_bedt5_3", img_button = "close_button.png", listname = "sw_bedt5_3", inv_size=6*2},
        not_in_creative_inventory=1
        
    },
    ["simple_wooden_bedside_table_6"] = {
        {mode="opened", button = "sw_bedt6_1", img_button = "close_button.png", listname = "sw_bedt6_1", inv_size=6*2},
        {mode="opened", button = "sw_bedt6_2", img_button = "close_button.png", listname = "sw_bedt6_2", inv_size=6*2},
        {mode="closed", button = "sw_bedt6_3", img_button = "open_button.png"},
        not_in_creative_inventory=1
        
    },
    ["simple_wooden_bedside_table_7"] = {
        {mode="closed", button = "sw_bedt7_1", img_button = "open_button.png"},
        {mode="opened", button = "sw_bedt7_2", img_button = "close_button.png", listname = "sw_bedt7_2", inv_size=6*2},
        {mode="opened", button = "sw_bedt7_3", img_button = "close_button.png", listname = "sw_bedt7_3", inv_size=6*2},
        not_in_creative_inventory=1
        
    },
    ["simple_wooden_bedside_table_8"] = {
        {mode="opened", button = "sw_bedt8_1", img_button = "close_button.png", listname = "sw_bedt8_1", inv_size=6*2},
        {mode="opened", button = "sw_bedt8_2", img_button = "close_button.png", listname = "sw_bedt8_2", inv_size=6*2},
        {mode="opened", button = "sw_bedt8_3", img_button = "close_button.png", listname = "sw_bedt8_3", inv_size=6*2},
        not_in_creative_inventory=1
        
    },
    inv_list = {{}, {}, {}} 
}

for bedside_t, bedside_ts in pairs(cabs_table["simple_wooden_bedside_table"]) do
  if bedside_t ~= "inv_list" then
    minetest.register_node("luxury_decor:"..bedside_t, {
        description = "Simple Wooden Bedside Table",
        visual_scale = 0.5,
        inventory_image = "simple_wooden_bedside_table_inv.png",
        mesh = bedside_t..".b3d",
        tiles = {"simple_bedside_table.png"},
        paramtype = "light",
        paramtype2 = "facedir",
        drop = "luxury_decor:simple_wooden_bedside_table_1",
        groups = {choppy=3, not_in_creative_inventory = bedside_ts["not_in_creative_inventory"]},
        drawtype = "mesh",
        collision_box = {
            type = "fixed",
            fixed = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}
        },
        selection_box = {
            type = "fixed",
            fixed = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}
        },
        sounds = default.node_sound_wood_defaults(),
        on_construct = function (pos)
            local name = minetest.get_node(pos).name
            local img_button1 = "image_button[0.5, 0;1, 2;" .. bedside_ts[1].img_button ..";" .. bedside_ts[1].button .. ";]"
            local img_button2 = "image_button[0.5, 2.5;1, 2;" .. bedside_ts[2].img_button .. ";" .. bedside_ts[2].button .. ";]"
            local img_button3 = "image_button[0.5, 5;1, 2;" .. bedside_ts[3].img_button .. ";" .. bedside_ts[3].button .. ";]"
            
            local y = 0
            local form = "size[9,11.5]" .. img_button1 .. img_button2 .. img_button3
            for num, drawer in pairs(bedside_ts) do
                if type(drawer) == "table" and drawer.mode == "opened" then
                    local str_pos = tostring(pos.x) .. ", " .. tostring(pos.y) .. ", " .. tostring(pos.z)
                    if not cabs_table["simple_wooden_bedside_table"].inv_list[num][str_pos] then
                       cabs_table["simple_wooden_bedside_table"].inv_list[num][str_pos] = {}
                   end
                   local list = "list[nodemeta:"..pos.x..","..pos.y..","..pos.z..";".. drawer.listname .. ";1.5,".. y .. ";6, 2]"
                   form = form .. list
                end
                y= y+2.5
            end
            
            form = form .. "list[current_player;main;0.5,7.5;8,4;]"
            local meta = minetest.get_meta(pos)
            meta:set_string("formspec", form)
            
            local inv = meta:get_inventory()
            for num2, drawer2 in pairs(bedside_ts) do
                if type(drawer2) == "table" and drawer2.inv_size ~= nil and drawer2.listname ~= nil then
                    local str_pos = tostring(pos.x) .. ", " .. tostring(pos.y) .. ", " .. tostring(pos.z)
                    inv:set_list(bedside_ts[num2].listname, cabs_table["simple_wooden_bedside_table"].inv_list[num2][str_pos])
                    inv:set_size(bedside_ts[num2].listname, bedside_ts[num2].inv_size)
                end
                
                
            end
            inv:set_size("main", 8*4)
        end,
        on_receive_fields = function (pos, formname, fields, sender)
            local name = minetest.get_node(pos).name
            local meta = minetest.get_meta(pos)
            local defined_mode = cabinets.define_mode(fields, name)
            local button_name
            for num, drawer in pairs(bedside_ts) do
                if type(drawer) == "table" then
                    local name = drawer.button
                    if fields[name] then
                        button_name = name
                        break
                    end
                end
            end
            
            if defined_mode == "closed" then
               cabinets.open(pos, "luxury_decor:" .. cabinets.define_needed_cabinet(fields, name), button_name, meta:get_string("formspec"), {"open_Drawer", "open_Drawer", "open_Drawer"})
            elseif defined_mode == "opened" then
               cabinets.close(pos, "luxury_decor:" .. cabinets.define_needed_cabinet(fields, name), button_name, meta:get_string("formspec"), {"close_Drawer", "close_Drawer", "close_Drawer"})
            end
            
                   
        end,
        after_dig_node = function (pos, oldnode, oldmetadata, digger)
            local name = string.sub(oldnode.name, 14)
            local generalized_name = string.sub(name, 1, -3)
              
            if cabs_table[generalized_name][name] then
                for num, drawer_lists in pairs(cabs_table[generalized_name].inv_list) do
                    for cab_pos, drawer_list in pairs(drawer_lists) do
                        local str_pos = tostring(pos.x) .. "," .. tostring(pos.y) .. "," .. tostring(pos.z)
                        if cab_pos == str_pos then
                            cabs_table[generalized_name].inv_list[num][cab_pos] = nil
                        end
                    end
                end
            end
                    
        end
    })
    
    if not bedside_ts.not_in_creative_inventory then
        minetest.register_craft({
            output = "luxury_decor:" .. bedside_t,
            recipe = {
                {"luxury_decor:pine_wooden_board", "luxury_decor:bedside_drawer", ""},
                {"luxury_decor:pine_wooden_board", "luxury_decor:bedside_drawer", ""},
                {"luxury_decor:pine_wooden_board", "luxury_decor:bedside_drawer", ""}
            }
        })
                
    end
  end
end

minetest.register_craftitem("luxury_decor:bedside_drawer", {
    description = "Bedside Drawer",
    inventory_image = "bedside_drawer.png",
    stack_max = 99
})

minetest.register_craft({
    output = "luxury_decor:bedside_drawer",
    recipe = {
        {"luxury_decor:pine_wooden_board", "luxury_decor:pine_wooden_board", ""},
        {"luxury_decor:pine_wooden_board", "", ""},
        {"default:stick", "", ""}
    }
})
    
