
                
minetest.register_node("luxury_decor:kitchen_wooden_chair", {
    description = "Kitchen Wooden Chair",
    visual_scale = 0.5,
    mesh = "kitchen_wooden_chair.obj",
    tiles = {"bright_wood_material.png"},
    paramtype = "light",
    paramtype2 = "facedir",
    groups = {choppy = 2.5},
    drawtype = "mesh",
    collision_box = {
        type = "fixed",
        fixed = {
            {-0.43, -0.45, -0.4, 0.43, 0.39, 0.35},
            {-0.43, -0.45, 0.35, 0.43, 1.3, 0.46}
            --[[{-0.65, -0.3, -1.46, 0.65, 1.4, -1.66},
            {-0.65, -0.3, 0.46, 0.65, 1.4, 0.66}]]
        }
    },
    selection_box = {
        type = "fixed",
        fixed = {
            {-0.43, -0.45, -0.4, 0.43, 0.39, 0.35},
            {-0.43, -0.45, 0.35, 0.43, 1.3, 0.46}
        }
    },
    sounds = default.node_sound_wood_defaults(),

        
})
 

minetest.register_node("luxury_decor:luxury_wooden_chair_with_cushion", {
    description = "Luxury Wooden Chair (with cushion)",
    visual_scale = 0.5,
    mesh = "luxury_wooden_chair_with_cushion.b3d",
    tiles = {"luxury_wooden_chair_with_cushion.png"},
    inventory_image = "luxury_wooden_chair_with_cushion_inv.png",
    paramtype = "light",
    paramtype2 = "facedir",
    groups = {choppy = 3.5},
    drawtype = "mesh",
    collision_box = {
        type = "fixed",
        fixed = {
                 {-0.45, -0.5, -0.45, 0.45, 0.28, 0.42},
                 {-0.3, 0.28, 0.28, 0.45, 1.4, 0.42}
            --[[{-0.65, -0.3, -1.46, 0.65, 1.4, -1.66},
            {-0.65, -0.3, 0.46, 0.65, 1.4, 0.66}]]
        }
    },
    selection_box = {
        type = "fixed",
        fixed = {
                 {-0.45, -0.5, -0.45, 0.45, 0.28, 0.42},
                 {-0.3, 0.28, 0.28, 0.45, 1.4, 0.42}
        }
    },
    sounds = default.node_sound_wood_defaults(),

})  


minetest.register_node("luxury_decor:decorative_wooden_chair", {
    description = "Decorative Wooden Chair",
    visual_scale = 0.5,
    mesh = "decorative_wooden_chair.b3d",
    inventory_image = "decorative_chair_inv.png",
    tiles = {"dark_wood_material2.png"},
    paramtype = "light",
    paramtype2 = "facedir",
    groups = {choppy = 2.5},
    drawtype = "mesh",
    collision_box = {
        type = "fixed",
        fixed = {
                 {-0.5, 0.36, 0.4, 0.5, 1.5, 0.5}, -- Upper box
                 {-0.5, -0.5, -0.5, 0.5, 0.29, 0.5}, -- Lower box
                 {-0.45, 0.29, -0.475, 0.45, 0.36, 0.4} -- Middle box
        }
    },
    selection_box = {
        type = "fixed",
        fixed = {
                 {-0.5, 0.36, 0.4, 0.5, 1.5, 0.5}, -- Upper box
                 {-0.5, -0.5, -0.5, 0.5, 0.29, 0.5}, -- Lower box
                 {-0.45, 0.29, -0.475, 0.45, 0.36, 0.4} -- Middle box
        }
    },
    sounds = default.node_sound_wood_defaults(),
    
})


minetest.register_node("luxury_decor:round_wooden_chair", {
    description = "Round Wooden Chair",
    visual_scale = 0.5,
    mesh = "round_wooden_chair.obj",
    tiles = {"bright_wood_material2.png"},
    paramtype = "light",
    paramtype2 = "facedir",
    groups = {choppy = 3.5},
    drawtype = "mesh",
    collision_box = {
        type = "fixed",
        fixed = {
            {-0.45, -0.5, -0.45, 0.45, 0.35, 0.45},
            {-0.45, 0.35, 0.2, 0.45, 1.45, 0.35}
            
            --[[{-0.65, -0.3, -1.46, 0.65, 1.4, -1.66},
            {-0.65, -0.3, 0.46, 0.65, 1.4, 0.66}]]
        }
    },
    selection_box = {
        type = "fixed",
        fixed = {
            {-0.45, -0.5, -0.45, 0.45, 0.35, 0.45},
            {-0.45, 0.35, 0.2, 0.45, 1.45, 0.35}
        }
    },
    
})

for _, material in ipairs({"", "jungle", "pine_"}) do
    for i = 1, 9 do
        minetest.register_craft({
            type = "shapeless",
            output = "luxury_decor:" .. material .. " wooden_plank",
            recipe = {"default:" .. material .. "wood"}
        })
    end
end

minetest.register_craft({
    output = "luxury_decor:kitchen_wooden_chair",
    recipe = {
        {"luxury_decor:wooden_plank", "default:stick", "default:stick"},
        {"luxury_decor:wooden_plank", "default:stick", ""},
        {"luxury_decor:wooden_plank", "default:stick", ""}
    }
})

minetest.register_craft({
    output = "luxury_decor:luxury_wooden_chair_with_cushion",
    recipe = {
        {"luxury_decor:jungle_wooden_plank", "default:stick", "default:stick"},
        {"luxury_decor:jungle_wooden_plank", "default:stick", "wool:white"},
        {"luxury_decor:jungle_wooden_plank", "default:stick", ""}
    }
})

minetest.register_craft({
    output = "luxury_decor:round_wooden_chair",
    recipe = {
        {"luxury_decor:pine_wooden_plank", "default:stick", "default:stick"},
        {"luxury_decor:pine_wooden_plank", "default:stick", ""},
        {"luxury_decor:pine_wooden_plank", "default:stick", ""}
    }
})

minetest.register_craft({
    output = "luxury_decor:decorative_wooden_chair",
    recipe = {
        {"luxury_decor:jungle_wooden_plank", "default:stick", "default:stick"},
        {"luxury_decor:jungle_wooden_plank", "default:stick", ""},
        {"luxury_decor:jungle_wooden_plank", "default:stick", ""}
    }
})
