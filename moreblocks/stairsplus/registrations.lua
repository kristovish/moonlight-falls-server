--[[
More Blocks: registrations

Copyright © 2011-2020 Hugo Locurcio and contributors.
Licensed under the zlib license. See LICENSE.md for more information.
--]]
local S = moreblocks.S
-- default registrations
if minetest.get_modpath("default") then
	local default_nodes = { -- Default stairs/slabs/panels/microblocks:
		"stone",
		"stone_block",
		"cobble",
		"mossycobble",
		"brick",
		"sandstone",
		"steelblock",
		"goldblock",
		"copperblock",
		"bronzeblock",
		"diamondblock",
		"tinblock",
		"desert_stone",
		"desert_stone_block",
		"desert_cobble",
		"meselamp",
		"glass",
		"tree",
		"wood",
		"jungletree",
		"junglewood",
		"pine_tree",
		"pine_wood",
		"acacia_tree",
		"acacia_wood",
		"aspen_tree",
		"aspen_wood",
		"obsidian",
		"obsidian_block",
		"obsidianbrick",
		"obsidian_glass",
		"stonebrick",
		"desert_stonebrick",
		"sandstonebrick",
		"silver_sandstone",
		"silver_sandstone_brick",
		"silver_sandstone_block",
		"desert_sandstone",
		"desert_sandstone_brick",
		"desert_sandstone_block",
		"sandstone_block",
		"coral_skeleton",
		"ice",
	}

	for _, name in pairs(default_nodes) do
		local mod = "default"
		local nodename = mod .. ":" .. name
		local ndef = table.copy(minetest.registered_nodes[nodename])
		ndef.sunlight_propagates = true

		-- Stone and desert_stone drop cobble and desert_cobble respectively.
		if type(ndef.drop) == "string" then
			ndef.drop = ndef.drop:gsub(".+:", "")
		end

		-- Use the primary tile for all sides of cut glasslike nodes and disregard paramtype2.
		if #ndef.tiles > 1 and ndef.drawtype and ndef.drawtype:find("glass") then
			ndef.tiles = {ndef.tiles[1]}
			ndef.paramtype2 = nil
		end

		mod = "moreblocks"
		stairsplus:register_all(mod, name, nodename, ndef)
		minetest.register_alias_force("stairs:stair_" .. name, mod .. ":stair_" .. name)
		minetest.register_alias_force("stairs:stair_outer_" .. name, mod .. ":stair_" .. name .. "_outer")
		minetest.register_alias_force("stairs:stair_inner_" .. name, mod .. ":stair_" .. name .. "_inner")
		minetest.register_alias_force("stairs:slab_"  .. name, mod .. ":slab_"  .. name)
	end
end

-- farming registrations
if minetest.get_modpath("farming") then
	local farming_nodes = {"straw"}
	for _, name in pairs(farming_nodes) do
		local mod = "farming"
		local nodename = mod .. ":" .. name
		local ndef = table.copy(minetest.registered_nodes[nodename])
		ndef.sunlight_propagates = true

		mod = "moreblocks"
		stairsplus:register_all(mod, name, nodename, ndef)
		minetest.register_alias_force("stairs:stair_" .. name, mod .. ":stair_" .. name)
		minetest.register_alias_force("stairs:stair_outer_" .. name, mod .. ":stair_" .. name .. "_outer")
		minetest.register_alias_force("stairs:stair_inner_" .. name, mod .. ":stair_" .. name .. "_inner")
		minetest.register_alias_force("stairs:slab_"  .. name, mod .. ":slab_"  .. name)
	end
end

-- wool registrations
if minetest.get_modpath("wool") then
	local dyes = {"white", "grey", "black", "red", "yellow", "green", "cyan",
	              "blue", "magenta", "orange", "violet", "brown", "pink",
	              "dark_grey", "dark_green"}
	for _, name in pairs(dyes) do
		local mod = "wool"
		local nodename = mod .. ":" .. name
		local ndef = table.copy(minetest.registered_nodes[nodename])
		ndef.sunlight_propagates = true

		stairsplus:register_all(mod, name, nodename, ndef)
	end
end

-- basic_materials, keeping the original other-mod-oriented names
-- for backwards compatibility

if minetest.get_modpath("basic_materials") then
	stairsplus:register_all("technic","concrete","basic_materials:concrete_block",{
		description = S("Concrete"),
		tiles = {"basic_materials_concrete_block.png",},
		groups = {cracky=1, level=2, concrete=1},
		sounds = moreblocks.node_sound_stone_defaults(),
	})

	minetest.register_alias("prefab:concrete_stair","technic:stair_concrete")
	minetest.register_alias("prefab:concrete_slab","technic:slab_concrete")

	stairsplus:register_all("gloopblocks", "cement", "basic_materials:cement_block", {
		description = S("Cement"),
		tiles = {"basic_materials_cement_block.png"},
		groups = {cracky=2, not_in_creative_inventory=1},
		sounds = moreblocks.node_sound_stone_defaults(),
		sunlight_propagates = true,
	})

	stairsplus:register_all("technic", "brass_block", "basic_materials:brass_block", {
		description= S("Brass Block"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"basic_materials_brass_block.png"},
	})

end

--Saltd
if minetest.get_modpath("saltd") then
	
	stairsplus:register_all("saltd", "salt_crystal_block", "saltd:salt_crystal_block", {
		description= S("Salt Crystal Block"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"saltd_salt_crystal_block.png"},
	})

	stairsplus:register_all("saltd", "salt_sand", "saltd:salt_sand", {
		description= S("Salt Sand"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"saltd_salt_sand.png"},
	})

end


--Lapis
if minetest.get_modpath("lapis") then
	
	stairsplus:register_all("lapis", "lapis_lapisblock", "lapis:lapisblock", {
		description= S("Lapislazuli Block"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"lapis_lapislazuliblock.png"},
	})

end

--Magicalities
if minetest.get_modpath("magicalities") then
	
	stairsplus:register_all("magicalities", "magicalities_silex", "magicalities:silex", {
		description= S("Silex Block"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"magicalities_silex.png"},
	})
    
    	stairsplus:register_all("magicalities", "magicalities_tellium_marble", "magicalities:tellium_marble", {
		description= S("Tellium Marble"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"magicalities_tellium_marble.png"},
	})

end


--australia
if minetest.get_modpath("australia") then
	
	stairsplus:register_all("australia", "bluestone_brick", "australia:bluestone_brick", {
		description= S("Bluestone Brick"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"aus_bluestone_brick.png"},
	})
    
    
    stairsplus:register_all("australia", "red_stonebrick", "australia:red_stonebrick", {
		description= S("Redstone Brick"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"aus_red_stonebrick.png"},
	})
    
    stairsplus:register_all("australia", "bluestone_cobble", "australia:bluestone_cobble", {
		description= S("Bluestone Cobble"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"aus_bluestone_cobble.png"},
	})
    
    
    stairsplus:register_all("australia", "red_cobble", "australia:red_cobble", {
		description= S("Redstone Cobble"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"aus_red_cobble.png"},
	})

end

--asteroid
if minetest.get_modpath("other_worlds") then
	
	stairsplus:register_all("asteroid", "stone_brick", "asteroid:stone_brick", {
		description= S("Asteroid Stone Brick"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"asteroid_stone_brick.png"},
	})
    
    
	stairsplus:register_all("asteroid", "redstone_brick", "asteroid:redstone_brick", {
		description= S("Asteroid Redtone Brick"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"asteroid_redstone_brick.png"},
	})
    
    stairsplus:register_all("asteroid", "cobble", "asteroid:cobble", {
		description= S("Asteroid Cobble"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"asteroid_cobble.png"},
	})


	stairsplus:register_all("asteroid", "stone", "asteroid:stone", {
		description= S("Asteroid Stone"),
		groups={cracky=1, not_in_creative_inventory=1},
		tiles={"asteroid_stone.png"},
	})
end




-- Alias cuts of split_stone_tile_alt which was renamed checker_stone_tile.
stairsplus:register_alias_all("moreblocks", "split_stone_tile_alt", "moreblocks", "checker_stone_tile")

-- The following LBM is necessary because the name stair_split_stone_tile_alt
-- conflicts with another node and so the alias for that specific node gets
-- ignored.
minetest.register_lbm({
	name = "moreblocks:fix_split_stone_tile_alt_name_collision",
	nodenames = {"moreblocks:stair_split_stone_tile_alt"},
	action = function(pos, node)
		minetest.set_node(pos, {
			name = "moreblocks:stair_checker_stone_tile",
			param2 = minetest.get_node(pos).param2

		})
		minetest.log('action', "LBM replaced " .. node.name ..
				" at " .. minetest.pos_to_string(pos))
	end,
})
