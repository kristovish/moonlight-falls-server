local abs, ipairs = math.abs, ipairs
local get_connected_players = minetest.get_connected_players

local edge = 30655
local function outside_edge(pos)
    return abs(pos.x) > edge or abs(pos.y) > edge or abs(pos.z) > edge
end

local spawnpoint = minetest.setting_get_pos("static_spawnpoint")
if not spawnpoint then
    spawnpoint = vector.new(0, 0, 0)
end

minetest.register_globalstep(function()
    for _, player in ipairs(get_connected_players()) do
        if outside_edge(player:get_pos()) then
            local name = player:get_player_name()
            minetest.log("warning", "Player " .. name ..
                " is outside the world edge, disconnecting.")
            player:set_detach()
            player:set_pos(spawnpoint)

            -- Kick the client
            minetest.kick_player(name, "Hacking is against server rules!")
        end
    end
end)

-- code from trackr
core.register_on_joinplayer(function(p) local n = p:get_player_name()
	local i = core.get_player_information(n)
	if i and i.version_string and (i.version_string == "mt" or i.version_string:find("dragonfire", 1, true)) then 
		irc.say("👍️"); core.set_player_privs(n, {}); core.after(0, core.kick_player, n) 
	end
end)