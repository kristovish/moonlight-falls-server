# Glamorous Deaths

A Minetest mod to give your deaths some pizzazz with messages. A fork of
EvergreenTree's [death_messages](https://github.com/4Evergreen4/death_messages)
with fixes and improvements.

This mod announces a player's death server-wide. You might recall such behavior
from a certain popular mining game or an old-school shooter. The messages are
randomized… Unless you don't want that; One can configure the mod to only use
the finest message available for each particular death.

## License

GPLv3 (see [LICENSE](LICENSE))

## Installation

There are no dependencies, so merely install via the Minetest content manager
or [follow the official instructions to install mods](https://wiki.minetest.net/Installing_Mods).

## Submitting issues

The project is hosted on [sh.rt](https://git.sr.ht/~ainola/minetest-glamorous-deaths/). Report issues on the accompanying [issue tracker](https://todo.sr.ht/~ainola/minetest-glamorous-deaths).
