if otherworlds.settings.crafting.enable then

	minetest.register_craft({
		output = "asteroid:cobble",
		recipe = {{"asteroid:stone"}}
	})

	minetest.register_craft({
		output = "asteroid:gravel",
		recipe = {{"asteroid:cobble"}}
	})

	minetest.register_craft({
		output = "asteroid:dust",
		recipe = {{"asteroid:gravel"}}
	})

	minetest.register_craft({
		type = "cooking",
		output = "asteroid:stone",
		recipe = "asteroid:cobble"
	})

	minetest.register_craft({
		output = "asteroid:redcobble",
		recipe = {{"asteroid:redstone"}}
	})

	minetest.register_craft({
		output = "asteroid:redgravel",
		recipe = {{"asteroid:redcobble"}}
	})

	minetest.register_craft({
		output = "asteroid:reddust",
		recipe = {{"asteroid:redgravel"}}
	})

	minetest.register_craft({
		type = "cooking",
		output = "asteroid:redstone",
		recipe = "asteroid:redcobble"
	})

	minetest.register_craft({
		output = "asteroid:redstone_brick 4",
		recipe = {
			{'asteroid:redstone','asteroid:redstone',''},
			{'asteroid:redstone','asteroid:redstone',''},
			{'','',''}
		}
	})

	minetest.register_craft({
		output = "asteroid:stone_brick 4",
		recipe = {
			{'asteroid:stone','asteroid:stone',''},
			{'asteroid:stone','asteroid:stone',''},
			{'','',''}
		}
	})
	
end
