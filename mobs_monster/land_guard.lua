
local S = mobs.intllib_monster


local guard_types = {

	{	nodes = {
			"default:snow", "default:snowblock", "default:ice",
			"default:dirt_with_snow"
		},
		skins = {"mobs_land_guard6.png", "mobs_land_guard7.png", "mobs_land_guard8.png"},
		drops = {
			{name = "default:ice", chance = 1, min = 1, max = 4},
			{name = "mobs:leather", chance = 2, min = 0, max = 2},
			{name = "default:diamond", chance = 4, min = 0, max = 2},
		},
	},

	{	nodes = {
			"ethereal:dry_dirt", "default:sand", "default:desert_sand",
			"default:dry_dirt_with_dry_grass", "default:dry_dirt"
		},
		skins = {"mobs_land_guard4.png", "mobs_land_guard5.png"},
		drops = {
			{name = "default:sandstone", chance = 1, min = 1, max = 4},
			{name = "mobs:leather", chance = 2, min = 0, max = 2},
			{name = "default:mese_crystal", chance = 4, min = 0, max = 2},
		},
	}
}

-- Land Guard

mobs:register_mob("mobs_monster:land_guard", {
	type = "npc",
	passive = false,
	attack_type = "dogfight",
    attacks_monsters = true,
	attack_npcs = false,
	owner_loyal = true,
	pathfinding = true,
	--group_attack = true,
	reach = 3,
	damage = 5,
	hp_min = 230,
	hp_max = 250,
	armor = 50,
	collisionbox = {-0.5, -1.01, -0.5, 0.5, 1.6, 0.5},
	visual_size = {x = 1, y = 1},
	visual = "mesh",
	mesh = "mobs_dungeon_master.b3d",
	textures = {
		{"mobs_land_guard.png"},
		{"mobs_land_guard2.png"},
		{"mobs_land_guard3.png"}
	},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_dungeonmaster",
	},
	walk_velocity = 1.5,
	run_velocity = 3.4,
	jump = true,
	jump_height = 2.0,
	floats = 0,
	view_range = 10,
	drops = {
		{name = "mobs:leather", chance = 2, min = 0, max = 2},
		{name = "default:mese_crystal", chance = 3, min = 0, max = 2},
		{name = "default:diamond", chance = 4, min = 0, max = 1},
	},
	water_damage = 0,
	lava_damage = 0,
	light_damage = 0,
	follow = {"default:goldblock", "farming:melon_8", "farming:pumpkin_8"},
	fear_height = 8,
	owner = "",
	order = "follow",
	animation = {
		stand_start = 0,
		stand_end = 19,
		walk_start = 20,
		walk_end = 35,
		punch_start = 36,
		punch_end = 48,
		speed_normal = 15,
		speed_run = 20,
	},


	on_rightclick = function(self, clicker)

		self.id = set_npc_id(self)

		-- feed to heal npc
		if mobs:feed_tame(self, clicker, 18, true, true) then return end

		-- capture npc with net or lasso
		if mobs:capture_mob(self, clicker, nil, 5, 80, false, nil) then return end

		-- protect npc with mobs:protector
		if mobs:protect(self, clicker) then return end

		local item = clicker:get_wielded_item()
		local name = clicker:get_player_name()

		
	end
})




if not mobs.custom_spawn_monster then
mobs:spawn({
	name = "mobs_monster:land_guard",
	nodes = {
		"default:snow", "default:ice", "default:stone",
		"default:dry_dirt_with_dry_grass", "ethereal:dry_dirt"
	},
	max_light = 7,
	chance = 250000,
	min_height = 0,
	active_object_count = 1,
})
end


mobs:register_egg("mobs_monster:land_guard", S("Land Guard"), "default_ice.png", 1)

