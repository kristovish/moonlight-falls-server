    
    local sdraia_list = {
	{ "Red sdraia", "red"},
	{ "Orange sdraia", "orange"},
    { "Black sdraia", "black"},
	{ "Yellow sdraia", "yellow"},
	{ "Green sdraia", "green"},
	{ "Blue sdraia", "blue"},
	{ "Violet sdraia", "violet"},
}

for i in ipairs(sdraia_list) do
	local sdraiadesc = sdraia_list[i][1]
	local colour = sdraia_list[i][2]
 


    
   minetest.register_node("summer:sdraia_"..colour.."", {
	    description = sdraiadesc.."",
	    drawtype = "mesh",
		mesh = "sdraia.obj",
	    tiles = {"sdraia_"..colour..".png",
	    },	    
        inventory_image = "sdraia_"..colour.."_inv.png",
	    
        wield_image  = {"sdraia_"..colour..".png" },
	    paramtype = "light",
	    paramtype2 = "facedir",
	    sunlight_propagates = true,
	    walkable = false,
	    selection_box = {
	        type = "fixed",
	        fixed = { 0.4, 0.1,1.0, -0.4,-0.49, -1.0 },
	    },
		groups = {snappy=2,cracky=3,oddly_breakable_by_hand=3,not_in_creative_inventory=0},
		--sounds = default.node_sound_wood_defaults(),
        drop = "summer:sdraia_"..colour.."",        
   })
   



end
