summer = {}

--local modpath = minetest.get_modpath("summer")

local modpath = minetest.get_modpath(minetest.get_current_modname())
summer.modpath = modpath


dofile(modpath.."/sdraia.lua")

dofile(modpath.."/porta.lua")

dofile(modpath.."/ombrellone_new.lua")

if minetest.get_modpath("3d_armor") then
dofile(modpath.."/occhiali.lua")
end

dofile(modpath.."/granite.lua")

dofile(modpath.."/craft.lua")

dofile(modpath.."/vetro.lua")

dofile(modpath.."/aliases.lua")

dofile(modpath.."/mattone.lua")

dofile(modpath.."/granite.lua")


