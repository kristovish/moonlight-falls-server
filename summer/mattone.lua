minetest.register_craftitem("summer:mattoneG", {
	description = "Mattone",
	inventory_image = "mattone.png",
	
})
    minetest.register_craftitem("summer:mattoneR", {
	description = "MattoneR",
	inventory_image = "mattoneR.png",
	
})
minetest.register_craftitem("summer:mattoneA", {
	description = "MattoneA",
	inventory_image = "mattoneA.png",
	
})    
    minetest.register_craftitem("summer:mattoneP", {
	description = "MattoneP",
	inventory_image = "mattoneP.png",
	
})    
--craftMATTONE

minetest.register_craft({
	type = 'cooking',
	recipe = "summer:pietraA",
	cooktime = 2,
	output = "summer:mattoneA",
})
minetest.register_craft({
	type = 'cooking',
	recipe = "summer:pietra",
	cooktime = 2,
	output = "summer:mattoneG",
})
minetest.register_craft({
	type = 'cooking',
	recipe = "summer:desert_pietra",
	cooktime = 2,
	output = "summer:mattoneR",
})
minetest.register_craft({
	type = 'cooking',
	recipe = "summer:pietraP",
	cooktime = 2,
	output = "summer:mattoneP",
})



    

    
    --craftitem PIETRA
minetest.register_craftitem("summer:desert_pietra", {
	description = "Desert Pietra",
	inventory_image = "desert_pietra.png",
	
})
    minetest.register_craftitem("summer:pietraA", {
	description = "pietraA",
	inventory_image = "pietraA.png",
	
})
minetest.register_craftitem("summer:pietra", {
	description = "pietra",
	inventory_image = "pietra.png",
	
})
    minetest.register_craftitem("summer:pietraP", {
	description = "pietraP",
	inventory_image = "pietraP.png",
	
})
   


   minetest.override_item('default:gravel', {
	description = "Gravel",
	tiles = {"default_gravel.png"},
	groups = {crumbly = 2, falling_node = 1},
	sounds = default.node_sound_gravel_defaults(),
	drop = {
		max_items = 2,
		items = {
			{items = {"default:flint"}, rarity = 3},
			{items = {"summer:desert_pietra"}, rarity = 3},
			{items = {"summer:pietraA"}, rarity = 3},
			{items = {"summer:pietra"}, rarity = 3},
			{items = {"summer:pietraP"}, rarity = 3},
			{items = {"default:gravel"}}
		}
	}
})

local c_lining = minetest.get_content_id("default:gravel")

minetest.register_alias("summer:gravel", "default:gravel")