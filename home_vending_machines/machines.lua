home_vending_machines.register_currency("currency:coin_10", 5)
home_vending_machines.register_currency("currency:coin_50", 1)

home_vending_machines.register_machine("simple", "home_workshop_misc:book_machine", {
    description = "book vending machine",
    tiles = {"home_vending_machines_book_machine.png"},
    sounds = nil,
    _vmachine = {
        item = "default:book"
    }
})

home_vending_machines.register_machine("simple", "home_vending_machines:drink_machine", {
    description = "Drinks vending machine",
    tiles = {"home_vending_machines_drink_machine.png"},
    sounds = nil,
    _vmachine = {
        item = 
        {"drinks:jbo_apple", "drinks:jbo_banana", "drinks:jbo_blueberry",
        "drinks:jbo_cactus", "drinks:jbo_carrot", "drinks:jbo_coconut",
        "drinks:jbo_cucumber", "drinks:jbo_grape", "drinks:jbo_apple",
        "drinks:jbo_melon", "drinks:jbo_orange", "drinks:jbo_pineapple",
        "drinks:jbo_pumpkin", "drinks:jbo_raspberry", "drinks:jbo_rhubarb",
        "drinks:jbo_strawberry", "drinks:jbo_tomato", "farming:rose_water"}
    }
})

home_vending_machines.register_machine("simple", "home_vending_machines:magic_machine", {
    description = "Sweets and Costume vending machine",
    tiles = {"home_vending_machines_sweet_machine.png"},
    sounds = nil,
    _vmachine = {
        item = 
        {"halloween:lolipop", "halloween:candycorn", "christmas_decor:candycane",
        "halloween:caramel_apple", "halloween:halloween_chocolate", "christmas_decor:gingerbread_man",
        "christmas_decor:gumdrop_yellow", "christmas_decor:gumdrop_orange", "christmas_decor:gumdrop_red" }
    }
})