if minetest.get_modpath("default") then
    if minetest.get_modpath("vessels") then
        minetest.register_craft({
            output = "home_vending_machines:drink_machine",
            recipe = {
                {"default:steel_ingot", "farming:rose_water", "default:steel_ingot"},
                {"default:steel_ingot", "basic_materials:motor", "default:steel_ingot"},
                {"default:steel_ingot", "default:copperblock", "default:steel_ingot"},
            },
        })
    end
    if minetest.global_exists("farming") and farming.mod == "redo" then
        minetest.register_craft({
            output = "home_vending_machines:magic_machine",
            recipe = {
                {"default:steel_ingot", "christmas_decor:gingerbread_man",    "default:steel_ingot"},
                {"default:steel_ingot", "basic_materials:motor", "default:steel_ingot"},
                {"default:steel_ingot", "default:copperblock", "default:steel_ingot"},
            },
        })
    end
end
