local function reg_item(name, evalue)
    minetest.register_craftitem("home_vending_machines:" .. name, {
        description = string.gsub(name, "_", " "),
        inventory_image = "home_vending_machines_" .. name .. ".png",
        on_use = minetest.item_eat(evalue),
    })
end

