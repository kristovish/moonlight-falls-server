# MultiDecor 1.0.0. Modpack For Minetest.
![logo](https://user-images.githubusercontent.com/25750346/185758916-9acf0ba5-5953-484f-825c-347c6ca7cddd.png)

## Description
This modpack adds various detailed furniture components, decorations and exterior stuff with various designs and styles of each epoch. Inspired by Homedecor and based on my abandoned Luxury Decor. Currently it contains the decor stuff only of "modern" style. A simple cheap style without any quaintness and luxury. In future there will be added still few sorts of that: baroque, high-tech, classic and royal.

## Changelog List
### [20.08.22] Release 1.0.0.
### [20.08.22] 1.0.0-beta.
### [10.01.22] The mod is born.

License (code & media): MIT.

Dependencies: default, wool, dye, xpanes, beds, flowers, moreores.

Minimal MT version supported: 5.6.0.
