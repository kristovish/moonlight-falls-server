local modpath = minetest.get_modpath("modern")

--dofile(modpath .. "/bedroom.lua")
--dofile(modpath .. "/chairs.lua")

dofile(modpath .. "/fences.lua")
dofile(modpath .. "/lamps.lua")
dofile(modpath .. "/living_room.lua")
dofile(modpath .. "/paintings.lua")

dofile(modpath .. "/tables.lua")

