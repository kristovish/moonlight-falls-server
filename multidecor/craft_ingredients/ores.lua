minetest.register_node(":multidecor:aluminum_ore", {
    description = "Aluminum Ore",
    tiles = {"default_stone.png^multidecor_aluminum_mineral.png"},
    is_ground_content = true,
    paramtype = "light",
    light_source = 2,
    drop = {
		max_items = 4,
		items = {
			{
				rarity = 1000,
				items = {"multidecor:aluminum_lump"}
			}
		}
	},
    groups = {cracky=2.5},
    sounds = default.node_sound_stone_defaults()
})
 

minetest.register_ore({
    ore_type = "scatter",
    ore = "multidecor:aluminum_ore",
    wherein = "default:stone",
    clust_scarcity = 800,
    clust_num_ores = 4,
    clust_size = 2,
    height_min = -31000,
    height_max = -150
})


minetest.register_craftitem(":multidecor:aluminum_lump",
{
	description = "aluminum Lump",
	inventory_image = "multidecor_aluminum_lump.png"
})

minetest.register_craftitem(":multidecor:aluminum_ingot",
{
	description = "Aluminum Ingot",
	inventory_image = "multidecor_aluminum_ingot.png"
})

minetest.register_craft({
	type = "cooking",
	output = "multidecor:aluminum_ingot",
	recipe = "multidecor:aluminum_ore",
	cooktime = 8
})

minetest.register_node(":multidecor:nickel_ore", {
    description = "Nickel Ore",
    tiles = {"default_stone.png^multidecor_nickel_mineral.png"},
    is_ground_content = true,
    paramtype = "light",
    light_source = 6,
    drop = {
		max_items = 5,
		items = {
			{
				rarity = 1000,
				items = {"multidecor:nickel_fragment"}
			}
		}
	},
    groups = {cracky=3.5},
    sounds = default.node_sound_stone_defaults()
})


minetest.register_ore({
    ore_type = "scatter",
    ore = "multidecor:nickel_ore",
    wherein = "default:stone",
    clust_scarcity = 400,
    clust_num_ores = 5,
    clust_size = 3,
    height_min = -31000,
    height_max = -125
})



minetest.register_craftitem(":multidecor:nickel_fragment",
{
	description = "Nickel Fragment",
	inventory_image = "multidecor_nickel_fragment.png"
})

minetest.register_craftitem(":multidecor:nickel_ingot",
{
	description = "Nickel Ingot",
	inventory_image = "multidecor_nickel_ingot.png"
})

minetest.register_craft({
	type = "cooking",
	output = "multidecor:nickel_ingot",
	recipe = "multidecor:nickel_fragment",
	cooktime = 5
})
