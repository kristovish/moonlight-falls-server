

minetest.register_craft({
	type = "shapeless",
	output = "currency:money_bundle",
	recipe = {
		"currency:coin_1",
		"currency:coin_10",
		"currency:coin_50",
		"currency:note_100",
		"currency:note_100",
		"currency:note_100",
		"currency:note_100",
		"currency:note_100",
		"currency:note_100",
	},
})

minetest.register_craft({
	type = "fuel",
	recipe = "currency:minegeld_bundle",
	burntime = 1,
})
