local S = minetest.get_translator("currency")


minetest.register_craftitem("currency:coin_1", {
	description = S("@1 Moonlight Coin", "1"),
	inventory_image = "coin.png",
		stack_max = 1000,
		groups = {minegeld = 1, minegeld_note = 1}
})


minetest.register_craftitem("currency:coin_10", {
	description = S("@1 Moonlight Coin", "10"),
	inventory_image = "coin_10.png",
		stack_max = 500,
		groups = {minegeld = 1, minegeld_note = 1}
})


minetest.register_craftitem("currency:coin_50", {
	description = S("@1 Moonlight Coin", "50"),
	inventory_image = "coin_50.png",
		stack_max = 250,
		groups = {minegeld = 1, minegeld_note = 1}
})

minetest.register_craftitem("currency:note_100", {
	description = S("@1 Moonlight Note", "100"),
	inventory_image = "note_100.png",
		stack_max = 100,
		groups = {minegeld = 1, minegeld_note = 1}
})

minetest.register_craftitem("currency:money_bundle", {
	description = S("Bundle of Money"),
	inventory_image = "bundle.png",
		stack_max = 100,
})
