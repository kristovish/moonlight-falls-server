minetest.register_privilege("ctf_admin", {
	description = "Manage administrative ctf settings/commands.",
	give_to_singleplayer = true,
	give_to_admin = true,
})
